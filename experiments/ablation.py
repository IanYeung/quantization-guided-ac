import csv
import argparse
import dataset
import torch
import dct
import model
from tqdm import tqdm
import sys
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Float, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship

# Widths are adjusted to keep *apx* the same number of parameters between all comparisons
experiments = [
    # Block vs frequency
    {
        'title': 'Blocknet vs frequencynet',
        'code': 'bvf',
        'configurations':[{
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 1,
                'feature_channels': 64
            }
        }, {
            'frequencynet': {
                'in_channels': 1,
                'out_channels': 1,
                'feature_channels': 1
            }
        }]
    },

    # block -> frequency vs frequency->block vs block->frequency->block
    {
        'title': 'structure test',
        'code': 'st',
        'configurations': [{
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 16,
                'feature_channels': 192
            },
            'frequencynet': {
                'in_channels': 16,
                'out_channels': 1,
                'feature_channels': 3
            }
        }, {
            'frequencynet': {
                'in_channels': 1,
                'out_channels': 16,
                'feature_channels': 3
            },
            'blocks_decode': {
                'in_channels': 16,
                'out_channels': 1,
                'feature_channels': 192
            },
        }, {
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 16,
                'feature_channels': 128
            },
            'frequencynet': {
                'in_channels': 16,
                'out_channels': 16,
                'feature_channels': 2
            },
            'blocks_decode': {
                'in_channels': 16,
                'out_channels': 1,
                'feature_channels': 128
            },
        }]
    },
    
    # Fusion vs no fusion
    {
        'title': 'Fusion test',
        'code': 'ft',
        'lr': 1e-4,
        'configurations': [{
            'title': 'Fusion test',
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 16,
                'feature_channels': 128
            },
            'frequencynet': {
                'in_channels': 16,
                'out_channels': 16,
                'feature_channels': 2
            },
            'blocks_decode': {
                'in_channels': 16,
                'out_channels': 1,
                'feature_channels': 128
            }
        }, {
            'title': 'Fusion test',
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 16,
                'feature_channels': 64
            },
            'frequencynet': {
                'in_channels': 16,
                'out_channels': 16,
                'feature_channels': 1
            },
            'blocks_decode': {
                'in_channels': 16,
                'out_channels': 16,
                'feature_channels': 64
            },
            'fusion': {
                'in_channels': [16, 16, 16],
                'fuse_channels': 1
            }
        }]
    },

    # CMF tests
    {
        'title': 'CMF',
        'code': 'cmf',
        'configurations':[{
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 1,
                'feature_channels': 64,
                'quantization': 'CMF'
            }
        }, {
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 1,
                'feature_channels': 64,
                'quantization': 'concat'
            }
        }, {
            'blocks_encode': {
                'in_channels': 1,
                'out_channels': 1,
                'feature_channels': 64,
                'quantization': 'none'
            }
        }]
    }
]

parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--qstats_path')
parser.add_argument('--dstats_path')
parser.add_argument('--run', nargs='+', required=False, default=['bvf', 'st', 'ft', 'cmf'])
parser.add_argument('--total_epochs', type=int)
parser.add_argument('--no_replace', action='store_true')
parser.add_argument('--database')
args = parser.parse_args()

engine = create_engine(args.database)
session = sessionmaker(bind=engine)()
Base = declarative_base()

class AblationExperiment(Base):
    __tablename__ = 'ablation_experiments'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    results = relationship('AblationResult', back_populates='experiment')

class AblationResult(Base):
    __tablename__ = 'ablation_results'

    id = Column(Integer, primary_key=True)
    
    configuration = Column(Integer)
    psnr = Column(Float, default=0)
    psnrb = Column(Float, default=0)
    ssim = Column(Float, default=0)
    experiment_id = Column(Integer, ForeignKey('ablation_experiments.id'))

    experiment = relationship('AblationExperiment', back_populates='results')

Base.metadata.create_all(engine)

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

training_qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
testing_qualities = [10]

dct_stats = dct.DCTStats(args.dstats_path, type='ms')
quantization_stats = dct.QuantizationStats(args.qstats_path, type='01')
training_quantized = dataset.JPEGQuantizedDataset(training_datasets, training_qualities, dct_stats, quantization_stats, crop=False, grayscale=False)
testing_quantized = dataset.JPEGQuantizedDataset(testing_datasets, testing_qualities, dct_stats, quantization_stats, crop=False, grayscale=True)

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=32, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=1, shuffle=False, num_workers=4)

device = torch.device('cuda')


def run_experiment(settings, n, lr=1e-3):
    deartifacting_network = model.DeartifactingNetwork(ablation_settings=settings).to(device) 
    optimizer = torch.optim.Adam(deartifacting_network.parameters(), lr=lr)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=len(training_loader) * args.total_epochs, eta_min=1e-5)

    psnr = torch.Tensor([0])
    for epoch in range(args.total_epochs):
        with tqdm(training_loader, desc='Train', unit='batch{{{}}}'.format(32)) as tt:
            for i, batch in enumerate(tt):
                initial_i = 0
                i = i + epoch * len(training_loader)
                l = model.train(i, deartifacting_network, device, batch, optimizer, None, stats=dct_stats, loss_function='error')

                tt.update()
                tt.set_postfix(loss=l.item())
                tt.set_postfix(learning_rate=optimizer.param_groups[0]['lr'], psnr=str(psnr.item()))

                scheduler.step()

                if i % 100 == 0:
                    psnr, _, _ = model.test(i, deartifacting_network, device, testing_loader, None, stats=dct_stats)
                    tt.set_postfix(learning_rate=optimizer.param_groups[0]['lr'], psnr=str(psnr.item()))

    psnr, psnrb, ssim = model.test(i, deartifacting_network, device, testing_loader, None, stats=dct_stats)

    return psnr.item(), psnrb.item(), ssim.item()

for experiment in experiments:
    if experiment['code'] in args.run:
        tqdm.write(experiment['title'])
        
        e = session.query(AblationExperiment).filter_by(title=experiment['title']).first()

        if e is None:
            e = AblationExperiment(title=experiment['title'])
            session.add(e)
            session.commit()

        for i, c in enumerate(experiment['configurations']):
            result = session.query(AblationResult).filter_by(configuration=i, experiment_id=e.id).first()

            if result is None:
                result = AblationResult(configuration=i, experiment_id=e.id)
            elif result is not None and args.no_replace:
                tqdm.write('Result already in database and no_replace is set, skipping')
                continue     
            
            lr = experiment['lr'] if 'lr' in experiment else 1e-3
            psnr, psnrb, ssim = run_experiment(c, i, lr=lr)                  

            result.psnr = psnr
            result.psnrb = psnrb
            result.ssim = ssim
            
            session.add(result)
            session.commit()

tqdm.write('Done all')