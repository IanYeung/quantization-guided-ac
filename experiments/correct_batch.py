import argparse
from pathlib import Path

import numpy as np
import torch
import torchjpeg
import torchvision
import tqdm
from PIL import Image
from skimage.color import rgb2ycbcr
from torchvision.transforms.functional import to_pil_image

import dataset
import dct
import metrics
import model
from dct import deblockify


def write_batch(output_spatial, paths, dataset, input_root, output_root):
    for i, p in enumerate(paths):
        source = Path(p)
        source = source.relative_to(input_root)
        output = output_root / source.with_suffix(".png")
        output.parent.mkdir(parents=True, exist_ok=True)
        torchvision.utils.save_image(output_spatial[i], str(output))


parser = argparse.ArgumentParser()
parser.add_argument("--input", type=Path)
parser.add_argument("--output", type=Path)
parser.add_argument("--dstats_path")
parser.add_argument("--color", action="store_true")
parser.add_argument("--weights")
parser.add_argument("--merp_from", required=False)
parser.add_argument("--merp_alpha", type=float, required=False)
parser.add_argument("--batch_size", type=int, default=1)
parser.add_argument("--debug_images", action="store_true")
args = parser.parse_args()

device = torch.device("cuda")
net = model.DeartifactingNetwork(color=args.color).to(device)

if args.merp_from is not None:
    model.checkpoint_merp(args.weights, args.merp_from, args.merp_alpha, net)
else:
    model.checkpoint_from_file(args.weights, net)

net.eval()

stats = dct.DCTStats(args.dstats_path, type="ms")

testing_quantized = dataset.FolderOfJpegDataset(str(args.input), stats)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=args.batch_size, shuffle=False, num_workers=0)

with torch.no_grad():
    for batch in tqdm.tqdm(testing_loader, desc="Correcting", unit="batch", leave=None):
        y, cb, cr, y_q, c_q, paths, dim = batch

        if args.debug_images:
            tqdm.tqdm.write(str(paths))

        y, cb, cr, y_q, c_q = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device)

        if y.shape[2] != cb.shape[2] * 2:
            y = torch.nn.functional.pad(y, [0, 0, 0, 8])

        if y.shape[3] != cb.shape[3] * 2:
            y = torch.nn.functional.pad(y, [0, 8, 0, 0])

        y_r, cb_r, cr_r = net(y_q, y, c_q, cb, cr)
        output = torch.cat([y_r, cb_r, cr_r], dim=1)

        output_spatial = dct.batch_to_images(output, device=device, stats=stats, crop=dim)

        write_batch(output_spatial, paths, testing_quantized, args.input, args.output)
