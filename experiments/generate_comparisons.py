# Requires comparisoncode submodule which is only available by request

import argparse
import subprocess
import tempfile
from pathlib import Path
from skimage.color import rgb2ycbcr
import numpy as np
from dataset import YChannel

from PIL import Image
from torchvision.transforms.functional import to_pil_image, to_tensor
import torch
from tqdm import tqdm

from metrics import metrics
import csv
import json
from skimage.color import rgb2ycbcr

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Float, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import sessionmaker, relationship

# TODO color still not working right

parser = argparse.ArgumentParser()
parser.add_argument('--datasets', nargs='+', type=Path)
parser.add_argument('--qualities', nargs='+', type=int)
parser.add_argument('--database')
parser.add_argument('--output_images', type=Path)
parser.add_argument('--models', nargs='+')
parser.add_argument('--path_to_code', type=Path)
parser.add_argument('--show_output', action='store_true')
parser.add_argument('--color', action='store_true')
parser.add_argument('--input_metrics', action='store_true')
parser.add_argument('--no_replace', action='store_true')
parser.add_argument('--gan', action='store_true')
parser.add_argument('--alpha', required=False, default=0.7, type=float)
parser.add_argument('--no_commit', action='store_true')
parser.add_argument('--model_quality', required=False, default=None, type=int)
args = parser.parse_args()

model_settings_y = {
    'ours': {
        'command': 'python experiments/correct_single.py --input "{input}" --output "{output}" --weights weights/weights_y.pt --dstats_path ./stats/ystats.pt --qstats_path ./stats/quantization_stats.pt'
    },
    'arcnn': {
        'command': 'matlab -nodisplay -r "cd(\\\"{path}\\\"); arcnn_at_quality(\\\"{input}\\\", {quality}, \\\"{output}\\\", false);exit"'
    },
    'idcn': {
        'command': 'cd "{path}/code" && python test_idcn_y.py --input "{input}" --output "{output}" --quality {quality}'
    },
    'mwcnn': {
        'command': 'matlab -nodisplay -r "cd(\\\"{path}\\\"); mwcnn_at_quality(\\\"{input}\\\", {quality}, \\\"{output}\\\", false);exit"'
    },
    'dmcnn': {
        'command': 'cd "{path}" && python test_single.py -t "{input}" -c ./weights/QF{quality}.ckpt -q {quality} -o "{output}"'
    },
    'jpeg': {
        'command': 'cp "{input}" "{output}"'
    }
}

model_settings_c = {
    'ours': {
        'command': 'python experiments/correct_single.py --input "{input}" --output "{output}" --weights weights/weights_c.pt --dstats_path ./stats/cstats2.pt --qstats_path ./stats/quantization_stats.pt --color --output_tensor'
    },
    'arcnn': {
        'command': 'matlab -nodisplay -r "cd(\\\"{path}\\\"); arcnn_at_quality(\\\"{input}\\\", {quality}, \\\"{output}\\\", true);exit"'
    },
    'idcn': {
        'command': 'cd "{path}/code" && python test_color_model.py --input "{input}" --output "{output}" --quality {quality} --model idcn'
    },
    'mwcnn': {
        'command': 'matlab -nodisplay -r "cd(\\\"{path}\\\"); mwcnn_at_quality(\\\"{input}\\\", {quality}, \\\"{output}\\\", true);exit"'
    },
    'dmcnn': {
        'command': 'cd "{path}" && python test_single.py -t "{input}" -c ./weights/QF{quality}.ckpt -q {quality} -o "{output}" --color'
    },
    'jpeg': {
        'command': 'cp "{input}" "{output}"'
    }
}

model_settings_g = {
    'ours': {
        'command': 'python experiments/correct_single.py --input "{input}" --output "{output}" --weights weights/weights_merp{alpha}.pt --dstats_path ./stats/cstats2.pt --qstats_path ./stats/quantization_stats.pt --color'
    }
}

engine = create_engine(args.database)
session = sessionmaker(bind=engine)()
Base = declarative_base()

class Model(Base):
    __tablename__ = 'models'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    results = relationship('Result', back_populates='model')

class Dataset(Base):
    __tablename__ = 'datasets'

    id = Column(Integer, primary_key=True)
    path = Column(String)

    results = relationship('Result', back_populates='dataset')

class Result(Base):
    __tablename__ = 'results'

    id = Column(Integer, primary_key=True)

    psnr = Column(Float, default=0)
    psnrb = Column(Float, default=0)
    ssim = Column(Float, default=0)

    color = Column(Boolean)    
    quality = Column(Integer)

    model_id = Column(Integer, ForeignKey('models.id'))
    dataset_id = Column(Integer, ForeignKey('datasets.id'))

    model = relationship('Model', back_populates='results')
    dataset = relationship('Dataset', back_populates='results')

    def __str__(self):
        return '{psnr:#.2f}|{psnrb:#.2f}|{ssim:#.3f}'.format(**self.__dict__)

    def __repr__(self):
        return '{psnr:#.2f}|{psnrb:#.2f}|{ssim:#.3f}'.format(**self.__dict__)

Base.metadata.create_all(engine)

if args.gan:
    model_settings = model_settings_g
elif args.color:
    model_settings = model_settings_c
else:
    model_settings = model_settings_y

with tempfile.TemporaryDirectory() as tmp:
    jpegdir = Path(tmp)
    
    print('Generating degraded inputs')
    for d in tqdm(args.datasets, desc='Datasets', leave=False):
        for q in tqdm(args.qualities, desc='Qualities', leave=False):
            output_path = jpegdir / str(q) / d.stem
            output_path.mkdir(parents=True, exist_ok=True)

            for i in tqdm(list(d.iterdir()), desc='Images', leave=False):
                with i.open('rb') as f:
                    im = Image.open(f)
                    im.load()

                if not args.color and not args.gan:
                    im = to_pil_image(YChannel()(to_tensor(im)))

                im.save(output_path / i.with_suffix('.jpg').name, quality=q)

    print('Running models')
    for d in tqdm(args.datasets, desc='Datasets', leave=False):
        dataset = session.query(Dataset).filter_by(path=str(d)).first()

        if dataset is None:
            dataset = Dataset(path=str(d))
            session.add(dataset)

            if not args.no_commit:
                session.commit()

        for m in tqdm(args.models, desc='Models', leave=False):
            model = session.query(Model).filter_by(name=m).first()

            if model is None:
                model = Model(name=m)
                session.add(model)
                
                if not args.no_commit:
                    session.commit()

            for q in tqdm(args.qualities, desc='Qualities', leave=False):
                model_code_path = args.path_to_code / m 
                input_path = jpegdir / str(q) / d.stem

                result = session.query(Result).filter_by(model_id=model.id, dataset_id=dataset.id, color=args.color, quality=q).first()

                if result is None:
                    result = Result(quality=q, color=args.color, dataset_id=dataset.id, model_id=model.id)
                elif result is not None and args.no_replace:
                    tqdm.write('Result already in database and no_replace is set, skipping')
                    continue

                result.psnr = 0
                result.psnrb = 0
                result.ssim = 0

                for i in tqdm(list(input_path.iterdir()), desc='Images', leave=False):       
                    output = args.output_images / m / str(q) / d.stem
                    output.mkdir(parents=True, exist_ok=True)
                    output = output.resolve()
                    output = output / i.with_suffix('.png').name

                    model_command = model_settings[m]['command'].format(**{
                        'path': model_code_path,
                        'input': i,
                        'quality': q if args.model_quality is None else args.model_quality,
                        'output': output,
                        'color': str(args.color).lower(),
                        'alpha': args.alpha
                    })

                    if args.show_output:
                        stderr = None
                        stdout = None
                        print('Running: {}'.format(model_command))
                    else:
                        stderr = subprocess.DEVNULL
                        stdout = subprocess.DEVNULL

                    subprocess.check_call(model_command, shell=True, stderr=stderr, stdout=stdout)

                    if m == 'ours' and args.color:
                        output_image = torch.load(str(output.with_suffix('.pt'))).unsqueeze(0)
                    else:
                        with output.open('rb') as f:
                            output_image = Image.open(f)
                            output_image.load()
                            output_image = to_tensor(output_image).unsqueeze(0)

                    input = next(x for x in d.iterdir() if x.stem == i.stem)
                    with input.open('rb') as f:
                        input_image = Image.open(f)
                        input_image.load()

                        if len(input_image.getbands()) > 1 and (not args.color and not args.gan):
                            input_image = Image.fromarray((rgb2ycbcr(np.array(input_image))[:, :, 0]).astype(np.uint8))

                        input_image = to_tensor(input_image).unsqueeze(0)
                        input_image = input_image[:, :, :output_image.shape[2], :output_image.shape[3]]
    
                    psnr, psnrb, ssim = metrics(input_image, output_image)
                    
                    result.psnr += psnr
                    result.psnrb += psnrb
                    result.ssim += ssim
                
                n = len(list(input_path.iterdir()))

                result.psnr /= n
                result.psnrb /= n
                result.ssim /= n

                session.add(result)
                
                if not args.no_commit:
                    session.commit()
