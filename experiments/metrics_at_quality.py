import torch
import dataset
import argparse
import dct
import metrics
import tqdm
import model
from torchvision.transforms.functional import to_pil_image

parser = argparse.ArgumentParser()
parser.add_argument('--images', nargs='+')
parser.add_argument('--quality', type=int)
parser.add_argument('--save_to')
parser.add_argument('--stats_path')
parser.add_argument('--qstats_path')
parser.add_argument('--checkpoint_path')
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--testing_batch_size', type=int, required=False, default=1)
args = parser.parse_args()

stats = dct.DCTStats(args.stats_path)
qstats = dct.QuantizationStats(args.qstats_path, type='01')
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.images]
testing_quantized = dataset.JPEGQuantizedDataset(testing_datasets, [args.quality], crop=False, dct_stats=stats, quantization_stats=qstats, macroblock_size=4)

print('{} testing examples'.format(len(testing_quantized)))

testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=args.testing_batch_size, shuffle=False, num_workers=4)

device = torch.device('cuda')
deartifacting_network = model.DeartifactingNetwork().to(device)
model.checkpoint_load(args.checkpoint_path, args.restart_from, deartifacting_network)

all_psnr = []
all_psnrb = []
all_ssim = []
n = 0

deartifacting_network.eval()

for degraded, quantization, target, size in tqdm.tqdm(testing_loader):
    with torch.no_grad():
        degraded, quantization, target = degraded.to(device), quantization.to(device), target.to(device)

        if degraded.shape[2] > 2000 or degraded.shape[3] > 2000:
            output = torch.zeros_like(degraded).cuda()

            n_blocks = torch.Tensor([degraded.shape[2] // 8, degraded.shape[3] // 8])
            blocks_size = (n_blocks // 4).long()
            
            for i in range(4):
                for j in range(4):
                    range_x = (torch.Tensor([i * blocks_size[1], (i + 1) * blocks_size[1]]) * 8).long()
                    range_y = (torch.Tensor([j * blocks_size[0], (j + 1) * blocks_size[0]]) * 8).long()

                    input_segment = degraded[:, :, range_y[0]:range_y[1], range_x[0]:range_x[1]]
                    output_segment = deartifacting_network(quantization, input_segment)
                    output[:, :, range_y[0]:range_y[1], range_x[0]:range_x[1]] = output_segment[:, :, :blocks_size[0] * 8, :blocks_size[1] * 8]                    
        else:
            output = deartifacting_network(quantization, degraded)

    target_spatial = dct.batch_to_images(target, crop=size, stats=stats, device=device)
    output_spatial = dct.batch_to_images(output, crop=size, stats=stats, device=device)

    psnr, psnrb, ssim = metrics.metrics(target_spatial, output_spatial, device=device)

    all_psnr.append(psnr * len(degraded))
    all_psnrb.append(psnrb * len(degraded))
    all_ssim.append(ssim * len(degraded))
    n += len(degraded)

    if args.save_to is not None:
        to_pil_image(output_spatial.cpu().squeeze()).save('{}/{}_restored.png'.format(args.save_to, n))
        to_pil_image(dct.batch_to_images(degraded, crop=size, stats=stats, device=device).cpu().squeeze()).save('{}/{}_degraded.png'.format(args.save_to, n))

all_psnr = sum(all_psnr) / n
all_psnrb = sum(all_psnrb) / n
all_ssim = sum(all_ssim) / n

print('Restored Metrics at Q={}: \tPSNR: {:#.2f}, PSNR-B: {:#.2f}, SSIM: {:#.3f}'.format(args.quality, all_psnr, all_psnrb, all_ssim))
