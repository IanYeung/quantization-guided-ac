import torch
import model
import argparse
from pathlib import Path
import dct
import dataset
from PIL import Image
from torchvision.transforms.functional import to_tensor, to_pil_image
from io import BytesIO
import metrics
import subprocess
import tempfile
from matplotlib import pyplot as plt
import torchvision
import csv

parser = argparse.ArgumentParser()
parser.add_argument('--quality', required=False, default=10, type=int)
parser.add_argument('--input', type=Path)
parser.add_argument('--output', type=Path)
args = parser.parse_args()

original_image = Image.open(args.input)
original_image_tensor = to_tensor(original_image)


def pad_to(channel, size):
    s = torch.Tensor(list(channel.shape))[2:]
    add_blocks = (torch.ceil(s / size) * size - s).long()

    channel = torch.nn.functional.pad(channel, [0, add_blocks[1], 0, add_blocks[0]], mode='replicate')
    return channel


def freq_domain(input):
    input = pad_to(input, 8)
    input = dct.to_ycbcr(input * 255)
    input -= 128
    return dct.batch_dct(input)[:, 0:1, :, :]


def vis_freq(f, name):
    input_vis = cm(f[0, 0, :, :]) * 255
    Image.fromarray(input_vis.astype('uint8')).save(args.output / name)

cm = plt.get_cmap('plasma')


args.output.mkdir(parents=True, exist_ok=True)

input_tensor = to_tensor(Image.open(args.input)).unsqueeze(0)
input_tensorf = freq_domain(input_tensor)

vis_freq(input_tensorf, 'original.png')

buffer = BytesIO()
to_pil_image(input_tensor.squeeze()).save(buffer, "JPEG", quality=args.quality)
buffer.seek(0)
compressed = to_tensor(Image.open(buffer)).unsqueeze(0)

compressedf = freq_domain(compressed)

vis_freq(compressedf, 'compressed.png')

with tempfile.TemporaryDirectory() as tmpd:
    tmpd = Path(tmpd)
    to_pil_image(input_tensor.squeeze()).save(tmpd / 'in.jpg', quality=args.quality)

    subprocess.check_call('python experiments/correct_single.py --input "{input}" --output "{output}" --weights ./weights/weights_c.pt --dstats_path ./stats/cstats2.pt --qstats_path ./stats/quantization_stats.pt --color'.format(input=tmpd / 'in.jpg', output=tmpd / 'out.png'), shell=True)

    restored_image = to_tensor(Image.open(tmpd / 'out.png')).unsqueeze(0)

restoredf = freq_domain(restored_image)

vis_freq(restoredf, 'regression.png')

with tempfile.TemporaryDirectory() as tmpd:
    tmpd = Path(tmpd)
    to_pil_image(input_tensor.squeeze()).save(tmpd / 'in.jpg', quality=args.quality)

    subprocess.check_call('python experiments/correct_single.py --input "{input}" --output "{output}" --weights ./weights/weights_g.pt --dstats_path ./stats/cstats2.pt --qstats_path ./stats/quantization_stats.pt --color'.format(input=tmpd / 'in.jpg', output=tmpd / 'out.png'), shell=True)

    gan_image = to_tensor(Image.open(tmpd / 'out.png')).unsqueeze(0)

ganf = freq_domain(gan_image)

vis_freq(ganf, 'gan.png')

def Z():
    z = torch.Tensor([[ 0,  1,  5,  6, 14, 15, 27, 28],
                      [ 2,  4,  7, 13, 16, 26, 29, 42],
                      [ 3,  8, 12, 17, 25, 30, 41, 43],
                      [ 9, 11, 18, 24, 31, 40, 44, 53],
                      [10, 19, 23, 32, 39, 45, 52, 54],
                      [20, 22, 33, 38, 46, 51, 55, 60],
                      [21, 34, 37, 47, 50, 56, 59, 61],
                      [35, 36, 48, 49, 57, 58, 62, 63]])

    Z_t = torch.zeros([8, 8, 64], dtype=torch.float)

    for alpha in range(8):
        for beta in range(8):
            for gamma in range(64):
                if z[alpha, beta] == gamma:
                    Z_t[alpha, beta, gamma] = 1

    return Z_t


def fnum():
    fn = torch.Tensor([0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 13, 13, 14])

    fn_t = torch.zeros([64, 15])

    for i in range(64):
        for j in range(15):
            if fn[i] == j:
                fn_t[i, j] = 1

    return fn_t

z = Z()
fn = fnum()

def count_freq(f):
    f = dct.blockify(f, 8).reshape(-1, 8, 8)
    flat = torch.einsum('mab,abg,gf->mf', f, z, fn).long()

    cnt = torch.zeros(f.shape[0], 15)
    for b in range(f.shape[0]):
        cnt[b] = torch.where(abs(flat[b]) > 0, torch.ones(15), torch.zeros(15))

    cnt = cnt.mean(dim=0)
    return cnt

cnt_original = count_freq(input_tensorf)
cnt_jpeg = count_freq(compressedf)
cnt_restored = count_freq(restoredf)
cnt_gan = count_freq(ganf)

with (args.output / 'report.csv').open('w', newline='') as fp:
    writer = csv.writer(fp)
    
    writer.writerow(['Frequency', 'Original', 'JPEG', 'Regression', 'GAN'])

    cnts = torch.stack([cnt_original, cnt_jpeg, cnt_restored, cnt_gan])
    cnts = cnts.transpose(1, 0)

    for f in range(15):
        writer.writerow([f, *(cnts[f].numpy())])
    