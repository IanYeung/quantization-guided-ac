import torch
import model
import argparse
from pathlib import Path
import dct
import dataset
from PIL import Image
from torchvision.transforms.functional import to_tensor
from io import BytesIO
import metrics
import subprocess
import tempfile
from tqdm import tqdm
import csv

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=Path)
parser.add_argument('--output', type=Path)
args = parser.parse_args()


quality_increases = torch.zeros(5)
size_decrease = torch.zeros(5)

for i, qi in enumerate(tqdm(range(10, 51, 10), leave=False)):
    for f in tqdm(list(args.dataset.iterdir()), leave=False):
        original_image = Image.open(f)
        original_image_tensor = to_tensor(original_image)

        with tempfile.TemporaryDirectory() as tmpd:
            tmpd = Path(tmpd)
            tmpd.mkdir(exist_ok=True, parents=True)

            original_image.save(tmpd / 'in.jpeg', quality=qi)
            jpeg_size = (tmpd / 'in.jpeg').stat().st_size

            subprocess.check_call('python experiments/correct_single.py --input "{input}" --output "{output}" --weights ./weights/weights_c.pt --dstats_path ./stats/cstats2.pt --qstats_path ./stats/quantization_stats.pt --color'.format(input=tmpd / 'in.jpeg', output=tmpd / 'out.png'), shell=True)

            restored_image = to_tensor(Image.open(tmpd / 'out.png'))


        target_e = metrics.ssim(original_image_tensor.unsqueeze(0), restored_image.unsqueeze(0)).mean()

        t = tqdm(range(qi + 1, 101), leave=False)
        for q in t:
            buffer = BytesIO()
            original_image.save(buffer, "JPEG", quality=q)
            buffer.seek(0)
            compressed = to_tensor(Image.open(buffer))

            e = metrics.ssim(original_image_tensor.unsqueeze(0), compressed.unsqueeze(0)).mean()

            if e >= target_e:
                tqdm.write('Equivilent quality {} at {} (restored: {})'.format(q, e, target_e))
                buffer.seek(0)

                quality_increases[i] += q - qi

                outfile = args.output / str(qi) / '{}_{}'.format(q, f.with_suffix('.jpg').name)
                outfile.parent.mkdir(exist_ok=True, parents=True)

                Image.open(buffer).save(outfile)

                hq_size = outfile.stat().st_size

                size_decrease[i] += hq_size - jpeg_size
                t.close()
                break

quality_increases /= len(list(args.dataset.iterdir()))
size_decrease /= len(list(args.dataset.iterdir()))

with (args.output / 'report.csv').open('w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['Quality', 'Quality Increase', 'Size Decrease'])
    for i, q in enumerate(range(10, 51, 10)):
        writer.writerow([q, quality_increases[i].item(), size_decrease[i].item()])