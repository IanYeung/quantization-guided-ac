import torch
import dataset
import model
import argparse
import sys
import tensorboardX
import os
import time
import dct
from tqdm import tqdm

torch.backends.cudnn.benchmark = True

parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--checkpoint_path')
parser.add_argument('--tag', required=False, default='jpeg-burnin')
parser.add_argument('--summary_path')
parser.add_argument('--stats_path')
parser.add_argument('--learning_rate', type=float, default=1e-4, required=False)
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--skip_optim', action='store_true', required=False)
parser.add_argument('--skip_scheduler', action='store_true', required=False)
parser.add_argument('--skip_run', action='store_true', required=False)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--autoencoder_checkpoint', default=None, required=True)
parser.add_argument('--autoencoder_restart_from', type=int, default=None, required=True)
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

training_qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
testing_qualities = [10]

stats = dct.DCTStats(args.stats_path)
training_quantized = dataset.JPEGQuantizedDataset(training_datasets, training_qualities, stats, crop=False, grayscale=False)
testing_quantized = dataset.JPEGQuantizedDataset(testing_datasets, testing_qualities, stats, crop=False)

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=args.batch_size, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=1, shuffle=False, num_workers=4)

device = torch.device('cuda')

generator = model.DeartifactingNetwork().to(device)

discriminator = model.Discriminator().to(device)
d_optimizer = torch.optim.Adam(discriminator.parameters(), lr=args.learning_rate)

run = time.ctime()

if args.restart_from is not None:
    print('Restarting with weights from {} with options:'.format(args.restart_from))
    print('\t[{}] Optimizer'.format(chr(9608) if not args.skip_optim else ' '))
    print('\t[{}] Scheduler'.format(chr(9608) if not args.skip_scheduler else ' '))
    print('\t[{}] Run folder'.format(chr(9608) if not args.skip_run else ' '))
    r = model.checkpoint_load(args.checkpoint_path,
                              args.restart_from,
                              generator,
                              None,
                              None,
                              discriminator,
                              d_optimizer if not args.skip_optim else None,
                              None)

    if not args.skip_run:
        run = r
        initial_i = args.restart_from + 1
    else:
        initial_i = 0
else:
    initial_i = 0

print('Loading autoencoder from {}/{}'.format(args.autoencoder_checkpoint, args.autoencoder_restart_from))
model.checkpoint_load(args.autoencoder_checkpoint,
                      args.autoencoder_restart_from,
                      generator)

if torch.cuda.device_count() > 1:
    print('Using {} GPUs'.format(torch.cuda.device_count()))
    generator = torch.nn.DataParallel(generator)
    discriminator = torch.nn.DataParallel(discriminator)

gan_loss = model.GanLoss()

print('Run at {}'.format(run))
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = tensorboardX.SummaryWriter(log_dir=run_path)

with tqdm(desc='Completed', initial=(initial_i - 1) // 1000, unit='epoch(s)') as tc:
    with tqdm(total=1000, desc='Epoch', initial=(initial_i - 1) % 1000, unit='batch{{{}}}'.format(args.batch_size)) as te:
        with tqdm(total=25, desc='Train', initial=(initial_i - 1) % 25, unit='batch{{{}}}'.format(args.batch_size)) as tt:
            for i in range(initial_i, sys.maxsize, 1):
                l = model.train_disc(i,
                                     generator,
                                     discriminator,
                                     device,
                                     training_loader,
                                     d_optimizer,
                                     summary_writer,
                                     stats=stats)

                tt.update()
                te.update()
                te.refresh()

                tt.set_postfix(loss=l.item())

                if i % 25 == 0:
                    if i % 1000 == 0:
                        te.disable = False
                        te.n = 0
                        te.refresh()

                        tc.update()

                    model.checkpoint_save(args.checkpoint_path, i, generator, None, None, run, discriminator, d_optimizer)
                    tc.set_postfix(last_checkpoint=i)

                    tt.disable = False
                    tt.n = 0
                    tt.refresh()
