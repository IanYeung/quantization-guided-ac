import argparse
import torch
import model
from tqdm import tqdm
import time

torch.backends.cudnn.benchmark=True

parser = argparse.ArgumentParser()
parser.add_argument('--n_frames', type=int)
parser.add_argument('--width', type=int)
parser.add_argument('--height', type=int)
args = parser.parse_args()


net = model.DeartifactingNetwork(color=True).cuda()
net.eval()

execution_time = 0.0

with torch.no_grad():
    batch_y = torch.zeros(1, 1, args.height, args.width).cuda()
    batch_cb = torch.zeros(1, 1, args.height//2, args.width//2).cuda()
    batch_cr = torch.zeros(1, 1, args.height//2, args.width//2).cuda()

    batch_y_q = torch.zeros(1, 1, 8, 8).cuda()
    batch_c_q = torch.zeros(1, 1, 8, 8).cuda()

    net(batch_y_q, batch_y, batch_c_q, batch_cb, batch_cr)

    for _ in tqdm(range(args.n_frames)):
        torch.cuda.synchronize()
        t0 = time.time()
        net(batch_y_q, batch_y, batch_c_q, batch_cb, batch_cr)
        torch.cuda.synchronize()
        execution_time += time.time() - t0

print('Total execution time {}'.format(execution_time))
print('Average FPS: {}'.format(args.n_frames / execution_time))
