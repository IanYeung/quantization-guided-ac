import torch
import dataset
import argparse
import dct
import metrics
import tqdm
from torchvision.utils import save_image

parser = argparse.ArgumentParser()
parser.add_argument('--images', nargs='+')
parser.add_argument('--quality', type=int)
parser.add_argument('--stats_path')
parser.add_argument('--qstats_path')
parser.add_argument('--color', action='store_true')
parser.add_argument('--testing_batch_size', type=int, required=False, default=1)
args = parser.parse_args()

stats = dct.DCTStats(args.stats_path)
qstats = dct.QuantizationStats(args.qstats_path, type='01')
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.images]
testing_quantized = dataset.JPEGQuantizedDataset(testing_datasets, [args.quality], crop=False, grayscale=(not args.color), dct_stats=stats, quantization_stats=qstats)

print('{} testing examples'.format(len(testing_quantized)))

testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=args.testing_batch_size, shuffle=False, num_workers=4)

all_psnr = []
all_psnrb = []
all_ssim = []
n = 0

for im in tqdm.tqdm(testing_loader):
    if args.color:
        y, cb, cr, y_q, c_q, target, size = im
        degraded = torch.cat([y, cb, cr], dim=1)
    else:
        degraded, quantization, target, size = im

    target_spatial = dct.batch_to_images(target, crop=size, stats=stats)
    input_spatial = dct.batch_to_images(degraded, crop=size, stats=stats)

    psnr, psnrb, ssim = metrics.metrics(target_spatial, input_spatial)

    all_psnr.append(psnr * len(degraded))
    all_psnrb.append(psnrb * len(degraded))
    all_ssim.append(ssim * len(degraded))
    n += len(degraded)

all_psnr = sum(all_psnr) / n
all_psnrb = sum(all_psnrb) / n
all_ssim = sum(all_ssim) / n

print('Input Metrics at Q={}: \tPSNR: {:.6f}, PSNR-B: {:.6f}, SSIM: {:.6f}'.format(args.quality, all_psnr, all_psnrb, all_ssim))