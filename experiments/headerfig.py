import torch
import dataset
import argparse
import dct
import metrics
import tqdm
from torchvision.utils import save_image
import model
import dct
from pathlib import Path
from dct import deblockify

parser = argparse.ArgumentParser()
parser.add_argument('--image')
parser.add_argument('--stats_path')
parser.add_argument('--qstats_path')
parser.add_argument('--output', type=Path)
parser.add_argument('--yweights')
parser.add_argument('--cweights')
parser.add_argument('--gweights')
args = parser.parse_args()

stats = dct.DCTStats(args.stats_path)
qstats = dct.QuantizationStats(args.qstats_path, type='01')
input_image_dataset = dataset.SingleImageDataset(args.image)
testing_quantized = dataset.JPEGQuantizedDataset([input_image_dataset], [10], crop=False, grayscale=False, dct_stats=stats, quantization_stats=qstats)
loader = torch.utils.data.DataLoader(testing_quantized, batch_size=1, shuffle=False)

device = torch.device('cuda')

args.output.mkdir(parents=True, exist_ok=True)


def extract_overlapping_blocks(channel, size):
    b = torch.nn.functional.unfold(channel, kernel_size=size, stride=size // 2, padding=size // 4)
    b = b.transpose(1, 2)
    b = b.view(-1, 1, size, size)
    return b


def pad_to(channel, size):
    s = torch.Tensor(list(channel.shape))[2:]
    add_blocks = (torch.ceil(s / size) * size - s).long()

    channel = torch.nn.functional.pad(channel, [0, add_blocks[1], 0, add_blocks[0]])
    return channel

print('Save JPEG')
for b in loader:
    y, cb, cr, y_q, c_q, target, size = b
    y, cb, cr, y_q, c_q, target = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device), target.to(device)

    y = dct.unprepare_dct(y, stats, device, 'y')
    cb = dct.unprepare_dct(cb, stats=stats, device=device, type='cb')
    cr = dct.unprepare_dct(cr, stats=stats, device=device, type='cr')

    output = torch.cat([y, dct.double_nn_dct(cb, True), dct.double_nn_dct(cr, True)], dim=1)

    output = dct.batch_to_images(output, stats, device, crop=size, scale_freq=False)
    save_image(output, args.output / 'input.png')

print('Correct Y Channel')
deartifacting_network = model.DeartifactingNetwork(color=False).to(device)
model.checkpoint_from_file(args.yweights, deartifacting_network)
deartifacting_network.eval()
with torch.no_grad():
    y, cb, cr, y_q, c_q, target, size = b
    y, cb, cr, y_q, c_q, target = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device), target.to(device)
    for b in loader:
        if y.shape[2] > 2000 or y.shape[3] > 2000:
            yp = pad_to(y, 256)

            y_r = []

            y_blocks = extract_overlapping_blocks(yp, 512)

            for b in range(y_blocks.shape[0]):
                is_y = y_blocks[b].unsqueeze(0)

                os_y = deartifacting_network(y_q, is_y)

                y_r.append(os_y[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))

            y_r = deblockify(torch.cat(y_r, dim=1), 1, (yp.shape[2], yp.shape[3]))
        else:
            y_r = deartifacting_network(y_q, y)

        y_r = dct.unprepare_dct(y_r, stats, device, 'y')
        cb = dct.unprepare_dct(cb, stats=stats, device=device, type='cb')
        cr = dct.unprepare_dct(cr, stats=stats, device=device, type='cr')

        output = torch.cat([y_r[:, :, :y.shape[2], :y.shape[3]], dct.double_nn_dct(cb, True), dct.double_nn_dct(cr, True)], dim=1)

        output = dct.batch_to_images(output, stats, device, crop=size, scale_freq=False)

        save_image(output, args.output / 'ychannel.png')

print('Correct Color Channels')
deartifacting_network = model.DeartifactingNetwork(color=True).to(device)
model.checkpoint_from_file(args.cweights, deartifacting_network)
deartifacting_network.eval()
with torch.no_grad():
    for b in loader:
        y, cb, cr, y_q, c_q, target, size = b
        y, cb, cr, y_q, c_q, target = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device), target.to(device)

        if y.shape[2] > 2000 or y.shape[3] > 2000:
            y = pad_to(y, 256)
            cb = pad_to(cb, 128)
            cr = pad_to(cr, 128)

            y_r = []
            cb_r = []
            cr_r = []

            y_blocks = extract_overlapping_blocks(y, 512)
            cb_blocks = extract_overlapping_blocks(cb, 256)
            cr_blocks = extract_overlapping_blocks(cr, 256)

            for b in range(y_blocks.shape[0]):
                is_y = y_blocks[b].unsqueeze(0)
                is_cb = cb_blocks[b].unsqueeze(0)
                is_cr = cr_blocks[b].unsqueeze(0)

                os_y, os_cb, os_cr = deartifacting_network(y_q, is_y, c_q, is_cb, is_cr)

                y_r.append(os_y[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))
                cb_r.append(os_cb[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))
                cr_r.append(os_cr[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))

            y_r = deblockify(torch.cat(y_r, dim=1), 1, (y.shape[2], y.shape[3]))
            cb_r = deblockify(torch.cat(cb_r, dim=1), 1, (y.shape[2], y.shape[3]))
            cr_r = deblockify(torch.cat(cr_r, dim=1), 1, (y.shape[2], y.shape[3]))
        else:
            y_r, cb_r, cr_r = deartifacting_network(y_q, y, c_q, cb, cr)

        output = torch.cat([y_r, cb_r, cr_r], dim=1)

        output = dct.batch_to_images(output, stats, device, crop=size)

        save_image(output, args.output / 'cchannel.png')

print('Correct GAN')
deartifacting_network = model.DeartifactingNetwork(color=True).to(device)
model.checkpoint_from_file(args.gweights, deartifacting_network)
deartifacting_network.eval()
with torch.no_grad():
    for b in loader:
        y, cb, cr, y_q, c_q, target, size = b
        y, cb, cr, y_q, c_q, target = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device), target.to(device)

        if y.shape[2] > 2000 or y.shape[3] > 2000:
            y = pad_to(y, 256)
            cb = pad_to(cb, 128)
            cr = pad_to(cr, 128)

            y_r = []
            cb_r = []
            cr_r = []

            y_blocks = extract_overlapping_blocks(y, 512)
            cb_blocks = extract_overlapping_blocks(cb, 256)
            cr_blocks = extract_overlapping_blocks(cr, 256)

            for b in range(y_blocks.shape[0]):
                is_y = y_blocks[b].unsqueeze(0)
                is_cb = cb_blocks[b].unsqueeze(0)
                is_cr = cr_blocks[b].unsqueeze(0)

                os_y, os_cb, os_cr = deartifacting_network(y_q, is_y, c_q, is_cb, is_cr)

                y_r.append(os_y[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))
                cb_r.append(os_cb[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))
                cr_r.append(os_cr[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))

            y_r = deblockify(torch.cat(y_r, dim=1), 1, (y.shape[2], y.shape[3]))
            cb_r = deblockify(torch.cat(cb_r, dim=1), 1, (y.shape[2], y.shape[3]))
            cr_r = deblockify(torch.cat(cr_r, dim=1), 1, (y.shape[2], y.shape[3]))
        else:
            y_r, cb_r, cr_r = deartifacting_network(y_q, y, c_q, cb, cr)

        output = torch.cat([y_r, cb_r, cr_r], dim=1)

        output = dct.batch_to_images(output, stats, device, crop=size)

        save_image(output, args.output / 'gan.png')        