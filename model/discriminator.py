import torch.nn
from .convolutional_filter_manifold import ConvolutionalFilterManifold
from .coherent_frequency_block import CoherentFrequencyBlock
from .weight_init import weight_init
from torch.nn.utils import spectral_norm

from dct import blockify


def dcgan_weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv2d') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0)


class Discriminator(torch.nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()

        self.block_y = ConvolutionalFilterManifold(1, 64, 8, 8)
        self.block_cb = ConvolutionalFilterManifold(1, 64, 8, 8)
        self.block_cr = ConvolutionalFilterManifold(1, 64, 8, 8)

        self.block_clean = torch.nn.Conv2d(3, 192, 8, 8)

        self.net = torch.nn.Sequential(            
            torch.nn.LeakyReLU(negative_slope=0.2, inplace=True),

            spectral_norm(torch.nn.Conv2d(384, 128, 1, 1, 0, bias=True)), 
            torch.nn.LeakyReLU(negative_slope=0.2, inplace=True),

            spectral_norm(torch.nn.Conv2d(128, 256, 1, 1, 0, bias=True)), 
            torch.nn.LeakyReLU(negative_slope=0.2, inplace=True),
 
            spectral_norm(torch.nn.Conv2d(256, 512, 1, 1, 0, bias=True)), 
            torch.nn.LeakyReLU(negative_slope=0.2, inplace=True),

            spectral_norm(torch.nn.Conv2d(512, 1, 1, 1, 0, bias=True))
        )

        self.apply(dcgan_weights_init)

    def forward(self, clean, compressed, q_y, q_c):
        y = self.block_y(q_y, compressed[:, 0:1, :, :])
        cb = self.block_cb(q_c, compressed[:, 1:2, :, :])
        cr = self.block_cr(q_c, compressed[:, 2:3, :, :])

        clean_blocks = self.block_clean(clean)

        return self.net(torch.cat([clean_blocks, y, cb, cr], dim=1)).view(-1, 1)

