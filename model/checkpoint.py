import torch
import os
from .merp import merp


def checkpoint_save(path, batch_idx, model, optimizer, scheduler, run, discriminator=None, d_optimizer=None, d_scheduler=None):
    os.makedirs(path, exist_ok=True)

    if type(model) == torch.nn.DataParallel:
        model = model.module

    if type(discriminator) == torch.nn.DataParallel:
        discriminator = discriminator.module

    save_path = os.path.join(path, 'checkpoint_{}.tar'.format(batch_idx))
    torch.save({
        'model_state': model.state_dict() if model is not None else None,
        'optimizer_state': optimizer.state_dict() if optimizer is not None else None,
        'scheduler_state': scheduler.state_dict() if scheduler is not None else None,
        'run': run,
        'discriminator': discriminator.state_dict() if discriminator is not None else None,
        'd_optimizer': d_optimizer.state_dict() if d_optimizer is not None else None,
        'd_scheduler': d_scheduler.state_dict() if d_scheduler is not None else None
    }, save_path)


def checkpoint_load(path, batch_idx, model, optimizer=None, scheduler=None, discriminator=None, d_optimizer=None, d_scheduler=None):
    load_path = os.path.join(path, 'checkpoint_{}.tar'.format(batch_idx))
    checkpoint = torch.load(load_path)
    model.load_state_dict(checkpoint['model_state'], strict=False)

    if optimizer is not None:
        optimizer.load_state_dict(checkpoint['optimizer_state'])

    if scheduler is not None:
        scheduler.load_state_dict(checkpoint['scheduler_state'])

    if discriminator is not None:
        discriminator.load_state_dict(checkpoint['discriminator'])

    if d_optimizer is not None:
        d_optimizer.load_state_dict(checkpoint['d_optimizer'])

    if d_scheduler is not None:
        d_scheduler.load_state_dict(checkpoint['d_scheduler'])

    return checkpoint['run']


def checkpoint_from_file(path, model):
    checkpoint = torch.load(path)
    model.load_state_dict(checkpoint['model_state'], strict=False)


def checkpoint_merp(patha, pathb, alpha, model):
    ca = torch.load(patha)['model_state']
    cb = torch.load(pathb)['model_state']
    merped = merp(ca, cb, alpha)
    model.load_state_dict(merped, strict=False)

