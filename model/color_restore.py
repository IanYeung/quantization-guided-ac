import torch.nn
from .convolutional_filter_manifold import ConvolutionalFilterManifold
from .coefficient_shuffler import CoefficientShuffler
from .rrdb import RRDB
from .perfrequency_convolution import PerFrequencyConvolution
from dct import double_nn_dct


class ColorRestore(torch.nn.Module):
    def __init__(self, channels, bias=True, activation=torch.nn.LeakyReLU, n_layers=1):
        super(ColorRestore, self).__init__()

        self.block_c = ConvolutionalFilterManifold(in_channels=1, out_channels=32, kernel_size=8, stride=8, activation=activation)
        self.a_c = activation()

        self.block_enhancer = torch.nn.Sequential(
            *[RRDB(kernel_size=3, channels=32, padding=1, bias=bias, activation=activation) for _ in range(n_layers)]
        )

        self.block_doubler = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=32, kernel_size=4, stride=2, padding=1, bias=True),
            activation()
        )

        self.block_y = ConvolutionalFilterManifold(in_channels=1, out_channels=32, kernel_size=8, stride=8, activation=activation)
        self.a_y = activation()

        self.block_enhancer2 = torch.nn.Sequential(
            *[RRDB(kernel_size=3, channels=64, padding=1, bias=bias, activation=activation) for _ in range(n_layers)]
        )
        

        self.unblock = ConvolutionalFilterManifold(in_channels=64, out_channels=1, kernel_size=8, stride=8, transposed=True, activation=activation)        

    def forward(self, q_y, q_c, y, c):
        c = self.a_c(self.block_c(q_c, c))
        d = self.block_enhancer(c)
        d = self.block_doubler(d)

        y = self.a_y(self.block_y(q_y, y))

        d = self.block_enhancer2(torch.cat([d, y], dim=1))

        d = self.unblock(q_c, d)

        return d


