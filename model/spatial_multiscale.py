import torch.nn
from .weight_init import weight_init


class DenseBlock(torch.nn.Module):
    def __init__(self, kernel_size, channels, conv_op, padding=0, stride=1, dilation=1, groups=1, scaler=0.2):
        super(DenseBlock, self).__init__()

        self.scaler = scaler

        self.conv1 = conv_op(in_channels=channels, out_channels=channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=True)
        self.relu1 = torch.nn.PReLU()
        self.conv2 = conv_op(in_channels=channels * 2, out_channels=channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=True)
        self.relu2 = torch.nn.PReLU()
        self.conv3 = conv_op(in_channels=channels * 3, out_channels=channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups,  bias=True)
        self.relu3 = torch.nn.PReLU()
        self.conv4 = conv_op(in_channels=channels * 4, out_channels=channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=True)
        self.relu4 = torch.nn.PReLU()
        self.conv5 = conv_op(in_channels=channels * 5, out_channels=channels, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=True)
        self.relu5 = torch.nn.PReLU()

    def forward(self, x):
        x1 = self.relu1(self.conv1(x))
        x2 = self.relu2(self.conv2(torch.cat((x, x1), 1)))
        x3 = self.relu3(self.conv3(torch.cat((x, x1, x2), 1)))
        x4 = self.relu4(self.conv4(torch.cat((x, x1, x2, x3), 1)))
        x5 = self.relu5(self.conv5(torch.cat((x, x1, x2, x3, x4), 1)))
        return x5 * self.scaler + x


class RRDB(torch.nn.Module):
    def __init__(self, kernel_size, channels, conv_op=torch.nn.Conv2d, padding=0, stride=1, dilation=1, groups=1):
        super(RRDB, self).__init__()
        self.scaler = 0.2

        self.block1 = DenseBlock(kernel_size=kernel_size, channels=channels, conv_op=conv_op, stride=stride, padding=padding, dilation=dilation, groups=groups, scaler=self.scaler)
        self.block2 = DenseBlock(kernel_size=kernel_size, channels=channels, conv_op=conv_op, stride=stride, padding=padding, dilation=dilation, groups=groups, scaler=self.scaler)
        self.block3 = DenseBlock(kernel_size=kernel_size, channels=channels, conv_op=conv_op, stride=stride, padding=padding, dilation=dilation, groups=groups, scaler=self.scaler)

    def forward(self, x):
        out = self.block1(x)
        out = self.block2(out)
        out = self.block3(out)

        return out * self.scaler + x


class SpatialMultiscale(torch.nn.Module):
    def __init__(self):
        super(SpatialMultiscale, self).__init__()

        self.encode = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=64, kernel_size=3, padding=1),
            torch.nn.PReLU()
        )

        self.net1 = torch.nn.Sequential(
            *[RRDB(kernel_size=3, channels=64, padding=1, dilation=1) for _ in range(5)]
        )

        self.net2 = torch.nn.Sequential(
            *[RRDB(kernel_size=3, channels=64, padding=2, dilation=2) for _ in range(5)]
        )

        self.net4 = torch.nn.Sequential(
            *[RRDB(kernel_size=3, channels=64, padding=4, dilation=4) for _ in range(5)]
        )

        self.net8 = torch.nn.Sequential(
            *[RRDB(kernel_size=3, channels=64, padding=8, dilation=8) for _ in range(5)]
        )

        self.decode = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=64*4, out_channels=1, kernel_size=3, padding=1),
            torch.nn.Tanh()
        )

        self.apply(lambda m: weight_init(scale=0.1, m=m))

    def forward(self, x):
        x = self.encode(x)

        x1 = self.net1(x)
        x2 = self.net2(x)
        x4 = self.net4(x)
        x8 = self.net8(x)

        x = self.decode(torch.cat([x8, x4, x2, x1], dim=1))

        return x
