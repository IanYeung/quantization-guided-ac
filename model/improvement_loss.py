def improvement_loss(output, target, input, loss, reduce=True):
    input_loss = loss(input, target, reduction='none')
    output_loss = loss(output, target, reduction='none')

    improvement = input_loss - output_loss

    if reduce:
        improvement = improvement.mean()

    return improvement
