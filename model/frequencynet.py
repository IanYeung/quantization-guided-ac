import torch.nn
import torch
from .coefficient_shuffler import CoefficientShuffler
from .safe_concat import concat_perfrequency
from .coherent_frequency_layer import CoherentFrequencyLayer
from .rrdb import RRDB
from .perfrequency_convolution import PerFrequencyConvolution



class FrequencyNet(torch.nn.Module):
    def __init__(self, in_channels, out_channels, n_layers=1, feature_channels=4, activation=torch.nn.LeakyReLU, bias=True):
        super(FrequencyNet, self).__init__()

        self.map = torch.nn.Sequential(
            CoefficientShuffler(channels=in_channels, direction='channels'),
            PerFrequencyConvolution(in_channels=in_channels, out_channels=feature_channels, kernel_size=3, padding=1, bias=bias),
            activation(),
            *[RRDB(kernel_size=3, channels=feature_channels, conv_op=PerFrequencyConvolution, padding=1, bias=bias, activation=activation) for _ in range(n_layers)],
            PerFrequencyConvolution(in_channels=feature_channels, out_channels=out_channels, kernel_size=3, padding=1, bias=bias),
            activation(),
            CoefficientShuffler(channels=out_channels, direction='blocks')
        )
                 

    def forward(self, x):
        return self.map(x)
