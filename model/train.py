import torch
from dataset import batch_to_images
from metrics import psnr, psnrb, ssim, blocking_effect_factor
from .improvement_loss import improvement_loss
from .frequency_loss import frequency_loss
from .quantization_loss import quantization_loss
from .frequency_task_loss import frequency_task_loss
import tqdm
from dct import double_nn_dct


def train(batch_idx, model, device, batch, optimizer, summary_writer, stats, loss_function='l1', color=False):
    model.train()
    if color:
        y, cb, cr, y_q, c_q, target, _ = batch
        y, cb, cr, y_q, c_q, target = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device), target.to(device)

        optimizer.zero_grad()
        y_r, cb_r, cr_r = model(y_q, y, c_q, cb, cr)

        input = torch.cat([y, double_nn_dct(cb, True), double_nn_dct(cr, True)], dim=1)
        output = torch.cat([y_r, cb_r, cr_r], dim=1)
    else:
        y, y_q, target, _ = batch
        y, y_q, target = y.to(device), y_q.to(device), target.to(device)

        optimizer.zero_grad()
        output = model(y_q, y)
        input = y

    loss = improvement_loss(output, target, input, torch.nn.functional.l1_loss, reduce=False)
    if loss_function == 'l1':
        loss = loss.mean()
    elif loss_function == 'ssim':
        target_spatial = batch_to_images(target, device=device, stats=stats)
        output_spatial = batch_to_images(output, device=device, stats=stats)

        loss = ssim(target_spatial, output_spatial, device=device).mean()
    elif loss_function == 'error':
        target_spatial = batch_to_images(target, device=device, stats=stats)
        output_spatial = batch_to_images(output, device=device, stats=stats)
        y_spatial = batch_to_images(input, device=device, stats=stats)

        l1_e = improvement_loss(output_spatial, target_spatial, y_spatial, torch.nn.functional.l1_loss, reduce=True)
        ssim_e = improvement_loss(y_spatial, target_spatial, output_spatial, lambda x, y, reduction: ssim(y, x, device=device), reduce=True)

        loss = l1_e + 0.05 * ssim_e
    elif loss_function == 'error2':
        target_spatial = batch_to_images(target, device=device, stats=stats)
        output_spatial = batch_to_images(output, device=device, stats=stats)
        y_spatial = batch_to_images(input, device=device, stats=stats)

        l1_e = improvement_loss(output_spatial, target_spatial, y_spatial, torch.nn.functional.l1_loss, reduce=True)
        bef_e = improvement_loss(output_spatial, target_spatial, y_spatial, lambda x, y, reduction: blocking_effect_factor(x), reduce=True)
        ssim_e = improvement_loss(y_spatial, target_spatial, output_spatial, lambda x, y, reduction: ssim(y, x, device=device), reduce=True)

        loss = l1_e + bef_e + 0.05 * ssim_e

    (-loss).backward()  # maximise l1 improvement <=> minimize negative l1 improvement
    optimizer.step()

    if summary_writer is not None:
        summary_writer.add_scalar('loss/total', global_step=batch_idx, scalar_value=loss)

        if loss_function == 'error':
            summary_writer.add_scalar('loss/ssim', global_step=batch_idx, scalar_value=ssim_e)
            summary_writer.add_scalar('loss/l1', global_step=batch_idx, scalar_value=l1_e)

    return loss
