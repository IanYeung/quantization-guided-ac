import dct
import torch


def frequency_loss(loss, device=None):
    f = torch.ones(8, 8) * torch.arange(8.)
    w = (f + f.t() + 1.) / 15.

    if device is not None:
        w = w.to(device)

    loss = dct.blockify(loss, 8)
    weighted_loss = loss * w

    return weighted_loss.mean()
