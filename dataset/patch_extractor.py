import argparse
import os
import torchvision
import dataset
import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--images', nargs='+')
parser.add_argument('--num_passes', type=int)
parser.add_argument('--output_path')
parser.add_argument('--color_type', choices=['color', 'luma', 'matlab_luma'])
parser.add_argument('--size', type=int)
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.images]

transform = [
    torchvision.transforms.RandomCrop(args.size),
]

if args.color_type == 'color':
    pass
elif args.color_type == 'luma':
    transform.append(torchvision.transforms.Grayscale())
elif args.color_type == 'matlab_luma':
    transform += [
        torchvision.transforms.ToTensor(),
        dataset.YChannel(),
        torchvision.transforms.ToPILImage()
    ]

transform = torchvision.transforms.Compose(transform)

os.makedirs(args.output_path, exist_ok=True)

total_images = args.num_passes * sum(len(d) for d in training_datasets)
file_format = '{{0:0{}}}.ppm'.format(len(str(total_images)))

print('Generating {} patches'.format(total_images))

n = 0
for d in tqdm.tqdm(training_datasets, desc='Dataset'):
    for i in tqdm.trange(len(d), desc='Image'):
        im = d[i]
        for _ in tqdm.trange(args.num_passes, desc='Pass'):
            p = transform(im)
            p.save(os.path.join(args.output_path, file_format.format(n)))
            n = n + 1
