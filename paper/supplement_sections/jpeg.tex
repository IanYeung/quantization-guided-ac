\section{JPEG Compression Algorithm}

Since the JPEG algorithm is core to the operation of our method, we describe it here in detail. Where the JPEG standard is ambiguous or lacking in guidance, we defer to the Independent JPEG Group's libjpeg software.

\subsubsection{Compression}

JPEG compression starts with an input image in RGB color space (for grayscale images the procedure is the same using only the Y channel equations) where each pixel uses the 8-bit unsigned integer represenation (\eg the pixel value is an integer in [0, 255]). The image is then converted to the YCbCr color space using the full 8-bit 
represenation (pixel values again in [0, 255], this is in contrast to the more common ITU-R BT.601 standard YCbCr color conversion) using the equations:
\begin{align}
    Y = 2.99R + 0.587B + 0.114G \\
    Cb = 128 - 0.168736R - 0.331264B + 0.5G \nonumber \\
    Cr = 128 + 0.5R - 0.418688B - 0.081312G \nonumber
\end{align}

Since the DCT will be taken on non-overlapping $8 \times 8$ blocks, the image is then padded in both dimensions to a multiple of 8. Note that if the color channels will be chroma subsampled, as is usually the case, then the image must be padded to the scale factor of the smallest channel times 8 or the subsampled channel will not be an even number of blocks. In most cases, chroma subsampling will be by half, so the image must be padded to a multiple of 16, this size is referred to as the minimum coded unit (MCU), or macroblock size. The padding is always done by repeating the last pixel value on the right and bottom edges. The chroma channels can now be subsampled.

Next the channels are centered around zero by subtracing 128 from each pixel, yielding pixel values in [-128, 127]. Then the 2D Discrete type 2 DCT is take on each non-overlapping $8 \times 8$ block as follows:
\begin{align}
    D_{i, j} = \frac{1}{4}C(i)C(j)\sum_{x=0}^7\sum_{y=0}^7P_{x,y}\cos\left[\frac{(2x+1)i\pi}{16}\right]\cos\left[\frac{(2y+1)j\pi}{16}\right] \\
    C(u) = \left\{\begin{array}{lr}
            \frac{1}{\sqrt{2}} & u = 0 \\
            1 & \text{otherwise}
        \end{array}\right. \nonumber
\end{align}
Where $D_{i,j}$ gives the coefficient for frequency $i, j$, and $P_{x,y}$ gives the pixel value for image plane $P$ at position pixel position $x,y$. Note that $C(u)$ is a scale factor that ensures the basis is orthonormal. 

The DCT coefficients can now be quantized. This follows the same procedure for the Y and color channels but with different quanitzation tables. We encourage readers to refer to the libjpeg software for details on how the quantization tables are computed given the scalar quality factor, an integer in [0, 100] (this is not a standardized process). Given the quantization tables $Q_Y$ and $Q_C$, the quanized coeffcients of each block are computed as:
\begin{align}
    Y'_{i, j} = \text{truncate}\left[\frac{Y_{i, j}}{Q_{Y_{i,j}}}\right] \\
    Cb'_{i, j} = \text{truncate}\left[\frac{Cb_{i, j}}{Q_{C_{i,j}}}\right] \nonumber \\ 
    Cr'_{i, j} = \text{truncate}\left[\frac{Cr_{i, j}}{Q_{C_{i,j}}}\right] \nonumber
    \label{eq:quant}
\end{align}
\
The quantized coefficients for each block are then vectorized (flattened) using a zig-zag ordering (see Figure \ref{fig:zig}) that is designed to place high frequencies further towards the end of the vectors. Given that high frequencies have lower magnitude and are more heavily quanitized, this usually creates a run of zeros at the end of each vector. The vectors are then compressed using run-length encoding on this final run of zeros (information prior to the final run is not run-length encoded.). The run-length encoded vectors are then entropy coded using either huffman coding or arithmetic coding and then written to the JPEG file along with associated metadata (EXIF tags), quantization tables, and huffman coding tables.

\begin{figure}
    \centering
    \begin{minipage}{0.4\linewidth}
    \resizebox{\columnwidth}{!}{
        \includegraphics{figures/zigzag.pdf}
    }
    \end{minipage}
    \caption{\textbf{Zigzag Ordering}}
    \label{fig:zig}
\end{figure}

\subsubsection{Decompression}

The decompression algorithm largely follows the reverse procedure of the compression algorithm. After reading the raw array data, huffman tables, and quantization tables, the entropy coding, run-length coding, and zig-zag ordering is reversed. We reiterate here that the JPEG file does not store a scalar quality from which the decompressor is expected to derive a quanitzation table, the decompressor reads the quanitzation table from the JPEG file and uses it directly, allowing any software to correctly decode JPEG files that were not written by it. 

Next, the $8 \times 8$ blocks are scaled using the quantization table:
\begin{align}
    Y_{i, j} = Y'_{i, j}Q_{Y_{i,j}} \\
    Cb_{i, j} = Cb'_{i, j}Q_{C_{i,j}} \nonumber \\ 
    Cr_{i, j} = Cr'_{i, j}Q_{C_{i,j}} \nonumber
\end{align}
There are a few things to note here. First, if dividing by the quantization table entry during compression (Equation \ref{eq:quant}) resulted in a fractional part (the result was not an integer), that fractional part was lost during truncation and the scaling here will recover an integer near to the true coefficient (how close it gets depends on the magnitude quantization table entry). Next, if the division in Equation \ref{eq:quant} resulted in a number in [0, 1), then that coeffient would be truncated to zero and is lost forever (it remains zero after this scaling process). This is the \textit{only} source of loss in JPEG compression, however it allows for the result to fit into integers instead of floating point numbers, and it creates larger runs of zeros which leads to significantly larger compression ratios.

Next, the DCT process for each block is reversed using the 2D Discrete type 3 DCT:
\begin{align}
    P_{x, y} = \frac{1}{4}\sum_{i=0}^7\sum_{j=0}^7C(i)C(j)D_{i,j}\cos\left[\frac{(2x+1)i\pi}{16}\right]\cos\left[\frac{(2y+1)j\pi}{16}\right] \\
    C(u) = \left\{\begin{array}{lr}
            \frac{1}{\sqrt{2}} & u = 0 \\
            1 & \text{otherwise}
        \end{array}\right. \nonumber
\end{align}
and the blocks are arranged in their correct spatial positions. The pixel values are uncentered (adding 128 to each pixel value), and the color channels are interpolated to their original size. Finally, the image is converted from YCbCr color space to RGB color space:
\begin{align}
R = Y + 1.402(Cr - 128) \\ 
G = Y - 0.344136(Cb - 128) -0.714136(Cr - 128) \nonumber \\
B = Y + 1.772(Cb - 128) \nonumber
\end{align}
and cropped to remove any block padding that was added during compression. The image is now ready for display.