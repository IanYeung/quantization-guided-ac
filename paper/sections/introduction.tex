\section{Introduction}

The JPEG image compression algorithm~\cite{wallace1992jpeg} is ubiquitous in modern computing. Thanks to 
its high compression ratios, it is extremely popular in bandwidth constrained applications. The JPEG algorithm is a lossy compression algorithm, so by 
using it, some information is lost for a corresponding gain in saved space. This is most noticable for low quality settings 

For highly space-constrained scenarios, it may be desirable to use aggressive compression. Therefore, 
algorithmic restoration of the lost information, referred to as artifact correction, has been well studied both in classical literature and in the context of deep neural networks.

While these methods have enjoyed academic success, their practical application is limited by a single 
architectural defect:  
they train a single model per JPEG quality level. The JPEG quality level is an 
integer between 0 and 100, where 100 indicates very little loss of information and 0 
indicates the maximum loss of information. Not only is this expensive to train and deploy, but the quality setting is not known at inference time (it 
is not stored with the JPEG image~\cite{wallace1992jpeg}) making it impossible to use these models in 
practical applications. Only recently have methods begun considering the ``blind'' restoration scenario \cite{kim2019pseudo, kim2020agarnet} with a single network, with mixed results compared to non-blind methods.

We solve this problem by creating a single model that uses quantization data, which is stored in the JPEG file. Our CNN model processes the image entirely in the DCT~\cite{ahmed1974discrete} domain. While 
previous works have recognized that the DCT domain is less likely to spread quantization errors 
\cite{wang2016d3, zhang2018dmcnn}, DCT domain-based models alone have historically not been successful unless 
combined with pixel domain models (so-called ``dual domain'' models). Inspired by recent methods 
\cite{ehrlich2019deep, deguerre2019fast,dong2015compression,gueguen2018faster}, we formulate fully DCT domain
regression. This allows our model to be parameterized by the quantization matrix, 
an $8 \times 8$ matrix that directly determines the quantization applied to each DCT coefficient. We develop
a novel method for parameterizing our network called Convolution Filter Manifolds, an 
extension of the Filter Manifold technique~\cite{kang2016crowd}. By adapting our network weights to the input 
quantization matrix, our single network is able to handle a wide range of quality settings. Finally, since JPEG images are stored in
the YCbCr color space, with the Y channel containing more information than the subsampled color channels, we
use the reconstructed Y channel to guide the color channel reconstructions. As in~\cite{zini2019deep}, we observe that using the Y channel in this way achieves good color correction
results. Finally, since regression results for artifact correction are often blurry, as a result of lost
texture information, we fine-tune our model using a GAN loss specifically designed to restore texture. This allows 
us to generate highly realistic reconstructions. See Figure \ref{fig:header} for an overview of the correction flow.
 
To summarize, our contributions are:
\begin{enumerate}
    \item A single model for artifact correction of JPEG images at any quality, parameterized by the 
    quantization matrix, which is state-of-the-art in color JPEG restoration.
    \item A formulation for fully DCT domain image-to-image regression.
    \item Convolutional Filter Manifolds for parameterizing CNNs with spatial side-channel information.
\end{enumerate}

\begin{figure}[t]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/header_2.pdf}
    \caption{\textbf{Correction process.} Excerpt from ICB RGB 8bit dataset ``hdr.ppm''. Input was compressed at quality 10.}
    \label{fig:header}
\end{figure}
