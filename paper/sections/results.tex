
\section{Experiments}

We validate the theoretical discussion in the previous sections with experimental results. We first describe the datasets
we used along with the training procedure we followed. We then show artifact correction results and compare them with previous state-of-the-art methods. Finally, we perform an ablation study.  Please see our supplementary material for further
results and details.

\begin{table}[t]
    \centering
    \caption{\textbf{Color Artifact Correction Results.} PSNR / PSNR-B / SSIM format. Best result in bold, second best underlined. JPEG column gives input error. For ICB, we used the RGB 8bit dataset.}
    \footnotesize    
    \renewcommand{\arraystretch}{1.2}
	\renewcommand{\tabcolsep}{1.2mm}
    \resizebox{\textwidth}{!}{
        \begin{tabular}{@{}l *{7}{c}@{}}
            \toprule
            Dataset & Quality & JPEG & ARCNN\cite{dong2015compression} &  MWCNN \cite{liu2018multi} & IDCN \cite{zheng2019implicit} & DMCNN \cite{zhang2018dmcnn} & Ours \\
            \midrule
            \multirow{3}{*}{Live-1} & \csvreader[late after line=\\]{data/comparisons/live1_color.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{BSDS500} & \csvreader[late after line=\\]{data/comparisons/BSDS500_color.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{ICB} & \csvreader[late after line=\\]{data/comparisons/ICB-RGB8.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow} 
            \bottomrule
        \end{tabular}
    }
    \label{tab:colorresults}
\end{table}

\subsection{Experimental Setup}

\noindent\textbf{Datasets and Metrics.} For training, we use the DIV2k and Flickr2k~\cite{agustsson2017ntire} datasets. DIV2k consists of 900 images, and the Flickr2k dataset contains 2650 images. We preextract $256 \times 256$ patches from these images taking 30 random patches from each image and compress them using quality in $[10, 100]$ in steps of 10. This gives a total training set of 1,065,000 patches.
For evaluation, we use the Live1~\cite{sheikh2006statistical, sheikh2006live}, Classic-5~\cite{foi2006pointwise}, 
BSDS500~\cite{arbelaez2010contour}, and ICB datasets~\cite{icb}. ICB is a new dataset which provides 15 high-quality lossless images designed specifically to measure compression quality. It is our hope that the community will gradually begin including ICB dataset results. Where previous works have provided code and models, we reevaluate their methods and provide results here for comparison. As with all prior works, we report PSNR, PSNR-B~\cite{tadala2012novel}, and SSIM~\cite{wang2004image}.

\noindent\textbf{Implementation Details.}  All training uses the Adam \cite{kingma2014adam} optimizer with a batch size of 32 patches. Our network is implemented using the PyTorch \cite{NEURIPS2019_9015} library. We normalize the DCT coefficients using per-frequency and per-channel mean and standard deviations. Since the DCT coefficients are measurements of different signals, by computing the statistics per-frequency we normalize the distributions so that they are all roughly the same magnitude. We find that this greatly speeds up the convergence of the network. Quantization table entries are
normalized to [0, 1], with 1 being the most quantization and 0 the least. We use libjpeg~\cite{libjpeg} for compression with the baseline quantization setting. 

\noindent\textbf{Training Procedure.} 
As described in Section~\ref{sec:app:train}, we follow a staged training approach by first training the Y channel or grayscale artifact correction network, then training the color (CbCr) channel network, and finally training both networks using the GAN loss.

For the first stage, the Y channel artifact correction network, the learning rate starts at $1 \times 10^{-3}$ and decays by a factor of 2 every 100,000 batches. We stop training after 400,000 batches. We set $\lambda$ in Equation~\ref{eq:regressionloss} to 0.05. 

For the next stage, all color channels are restored. The weights for the Y channel network are initialized from the previous stage and frozen during training. The color channel network weights are trained using a cosine annealing learning rate schedule \cite{loshchilov2016sgdr} decaying from $1 \times 10^{-3}$ to $1 \times 10^{-6}$ over 100,000 batches. 

Finally, we train both Y and color channel artifact correction networks (jointly referred to as the generator model) using a GAN loss to improve qualitative textures. The generator model weights are initialized to the pre-trained models from the previous stages. We use the DCGAN \cite{radford2015unsupervised} discriminator. The model is trained for 100,000 iterations using cosine annealing~\cite{loshchilov2016sgdr}
with the learning rate starting from $1 \times 10^{-4}$ and ending at $1 \times 10^{-6}$.  We set $\gamma$ and $\nu$ in Equation~ \ref{eq:ganloss} to $5 \times 10^{-3}$ and $1 \times 10^{-2}$ respectively.  

\subsection{Results: Artifact Correction}

\noindent\textbf{Color Artifact Correction.} 
We report the main results of our approach, color artifact correction, on Live1, BSDS500, and ICB in Table~\ref{tab:colorresults}. Our model consistently outperforms recent baselines on all datasets. Note that of all the approaches, only ours and IDCN~\cite{zheng2019implicit} include native processing of color channels. For the other models, we convert input images to YCbCr and process the channels independently. 

For quantitative comparisons to more methods on Live-1 dataset, at compression quality 10, refer to Figure \ref{fig:compare}. We present qualitative results from a mix of all three datasets in Figure~\ref{fig:gan} (``Ours''). Since our model is not restricted by which quality settings it can be run on, we also show the increase in PSNR for qualities 10-100 in Figure \ref{fig:ipsnr}.

\begin{table}[t]
    \centering
    \caption{\textbf{Y Channel Correction Results.} PSNR / PSNR-B / SSIM format, the best result is highlighted in bold, second best is underlined. The JPEG column gives with input error of the images. For ICB, we used the Grayscale 8bit dataset. We add Classic-5, a grayscale only dataset.}
    \footnotesize    
    \renewcommand{\arraystretch}{1.2}
	\renewcommand{\tabcolsep}{1.2mm}
    \resizebox{\textwidth}{!}{
        \begin{tabular}{@{}l *{7}{c}@{}}
            \toprule
            Dataset & Quality & JPEG & ARCNN\cite{dong2015compression} &  MWCNN \cite{liu2018multi} & IDCN \cite{zheng2019implicit} & DMCNN \cite{zhang2018dmcnn} & Ours \\
            \midrule
            \multirow{3}{*}{Live-1} & \csvreader[late after line=\\]{data/comparisons/live1.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{Classic-5} & \csvreader[late after line=\\]{data/comparisons/classic5.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{BSDS500} & \csvreader[late after line=\\]{data/comparisons//BSDS500.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{ICB} & \csvreader[late after line=\\]{data/comparisons//ICB-GRAY8.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \bottomrule
        \end{tabular}
    }
    \label{tab:yresults}
\end{table}


\begin{figure}[t]
    \centering
    \resizebox{0.85\columnwidth}{!}{
    \begin{tabular}{ccccc}
        Original & JPEG & IDCN Q=10 & IDCN Q=20 & Ours \\
        \includegraphics[width=0.2\linewidth]{figures/generalize/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/generalize/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/generalize/idcn_10.png} &
        \includegraphics[width=0.2\linewidth]{figures/generalize/idcn_20.png} & 
        \includegraphics[width=0.2\linewidth]{figures/generalize/ours.png} 
    \end{tabular}}
    \caption{\textbf{Generalization Example}. Input was compressed at quality 50. Please zoom in to view details.}
    \label{fig:gen}
\end{figure}


\begin{table}[t]
    \centering
    \caption{\textbf{Generalization Capabilities}. Live-1 dataset (PSNR / PSNR-B / SSIM).}
    \footnotesize    
    \renewcommand{\arraystretch}{1.2}
	\renewcommand{\tabcolsep}{1.2mm}
    \resizebox{0.85\columnwidth}{!}{
        \begin{tabular}{@{} *{5}{c}@{}}
            \toprule
            Model Quality & Image Quality & JPEG & IDCN \cite{zheng2019implicit} & Ours \\
            \midrule
            %
            %\csvreader[late after line=\\]{data/generalization/live1_generalize.csv}{}{\csvlinetotablerow} 
10 & \multirow{2}{*}{50} & 30.91 / 28.94 / 0.905 & 30.19 / 30.14 / 0.889 & \multirow{2}{*}{\textbf{32.78 / 32.19 / 0.932}} \\
20 & & 30.91 / 28.94 / 0.905 & 31.91 / 31.65 / 0.916 & \\
\midrule
10 & 20 & 27.96 / 25.77 / 0.837 & 29.25 / 29.08 / 0.863 & \textbf{29.92 / 29.51 / 0.882} \\
20 & 10 & 25.60 / 23.53 / 0.755 & 26.95 / 26.24 / 0.804 & \textbf{27.65 / 27.40 / 0.819} \\
            \bottomrule
        \end{tabular}
    }
    \label{tab:genresults}
\end{table}


\begin{figure}[t]
    \begin{minipage}[t][][b]{0.48\linewidth}
        \centering
        \includegraphics[height=0.7\linewidth]{plots/increase.pdf}
        \caption{\textbf{Increase in PSNR on color datasets.} For all three datasets we show the average improvement in PSNR values on qualities 10-100. Improvement drops off steeply at quality 90.}
        \label{fig:ipsnr}
    \end{minipage}
    %\hspace{0.02\linewidth}
    \hfill
    \begin{minipage}[t][][b]{0.48\linewidth}
        \centering
        \includegraphics[height=0.65\linewidth]{plots/compare.pdf}
        \caption{\textbf{Comparison for Live-1 quality 10.} Where
        code was available we reevaluated, otherwise we used published numbers.}
        \label{fig:compare}
    \end{minipage}
\end{figure}


\begin{figure}[t]
    \centering
    \resizebox{0.85\columnwidth}{!}{
    \begin{tabular}{ccccccc}
        Original & JPEG & IDCN & Ours & Ours-GAN \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/original.jpg} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/ours.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/ours.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/gan.png} \\
    \end{tabular}}
    \caption{\textbf{Qualitative Results.} All images were compressed at Quality 10. Please zoom in to view details.}
    \label{fig:gan}
\end{figure}


\begin{table}[t]
    \footnotesize    
    \renewcommand{\arraystretch}{1.1}
    \renewcommand{\tabcolsep}{1mm}
  	\centering
    \begin{minipage}[t][][b]{0.47\linewidth}
        \centering
        \caption{\textbf{GAN FID Scores.}}
        \resizebox{0.6\columnwidth}{!}{
        \begin{tabular}{@{}l *{3}{c}@{}}
            \toprule
            Dataset & Quality & Ours & Ours-GAN \\
            \midrule 
            \multirow{3}{*}{Live-1} & \csvreader[late after line=\\]{data/fid/fid_live1.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{BSDS500} & \csvreader[late after line=\\]{data/fid/fid_bsds500.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{ICB} & \csvreader[late after line=\\]{data/fid/fid_icb.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \bottomrule
        \end{tabular} }
        \label{tab:fid}
    \end{minipage}
    \hfill
    \begin{minipage}[t][][b]{0.51\linewidth}
        \centering
        \caption{\textbf{Ablation Results.} (refer to Section~\ref{sec:res:ablation} for details).}
        \resizebox{0.75\columnwidth}{!}{
        \begin{tabular}{@{}L{1.8cm} L{1.8cm} *{7}{c}@{}}
            \toprule
            Experiment & Model & PSNR & PSNR-B & SSIM \\
            \midrule
            \multirow{3}{*}{CFM} & \csvreader[late after line=\\]{data/ablation/cmf.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{2}{*}{Subnetworks} & \csvreader[late after line=\\]{data/ablation/bvf.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{2}{*}{Fusion} & \csvreader[late after line=\\]{data/ablation/ft.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \bottomrule
        \end{tabular} }  
        \label{tab:ablation}
\end{minipage}
\end{table}

\noindent\textbf{Intermediate Results on Y Channel Artifact Correction.}
Since the first stage of our approach trains for grayscale or Y channel artifact correction, we can also compare the intermediate results from this stage with other approaches. We report results in Table~\ref{tab:yresults} for Live1, Classic-5, BSDS500, and ICB. As the table shows, intermediate results from our model can match or outperform previous state-of-the-art models in many cases, consistently providing high SSIM results using a single model for all quality factors.

\noindent\textbf{GAN Correction}
\label{sec:res:gan}
Finally, we show results from our model trained using GAN correction. We use model interpolation~\cite{wang2018esrgan}
and show qualitative results for the interpolation parameter ($\alpha$) set to 0.7 in Figure \ref{fig:gan}. (``Ours-GAN'') Notice that the GAN loss is able to restore texture to blurred, flat regions and sharpen edges, yielding a more visually pleasing result. We provide additional qualitative results in the supplementary material. Note that we do not show error metrics using the GAN model as it produces higher quality images, at the expense of quantitative metrics, by adding texture details that are not present in the original images. We instead show
FID scores for the GAN model compared to our regression model in Table~\ref{tab:fid}, indicating that the GAN model generates significantly more 
realistic images.

\subsection{Results: Generalization Capabilities}
The major advantage of our method is that it uses a single model to correct JPEG images at any quality, while prior works train a model for each quality factor. Therefore, we explore if other methods are capable of generalizing or if they really require this ensemble of quality-specific models. To evaluate this, we use our closest competitor and prior state-of-the-art, IDCN~\cite{zheng2019implicit}. IDCN does not provide a model for quality higher than 20, we explore if their model generalizes by using their quality 10 and quality 20 models to correct
quality 50 Live-1 images. We also use the quality 20 model to correct quality 10 images and use the quality
10 model to correct quality 20 images. These results are shown in Table~\ref{tab:genresults} along with our result.

As the table shows, the choice of model is critical for IDCN, and there is a significant quality drop when choosing the wrong model. Neither their quality 10 nor their quality 20 model is able to effectively correct images that it was not trained on, scoring significantly lower than if the correct model were used. At quality 50, the quality 10 model produces a result worse than the input JPEG, and the quality 20 model makes only a slight improvement. In comparison, our single model provides consistently better results across image quality factors. We stress that the quality setting is not stored in the JPEG file, so a deployed system has no way to pick the correct model. We show an example of a quality 50 image and artifact correction results in Figure \ref{fig:gen}.

\subsection{Design and Ablation Analysis}
\label{sec:res:ablation}
Here we ablate many of our design decisions and observe their effect on network accuracy. The results are reported in Table~\ref{tab:ablation}, we report metrics on quality 10 classic-5. 

\noindent\textbf{Implementation details:}\smallskip For all ablation experiments, we keep the number of parameters approximately the same between tested models to alleviate the concern that a network performs better simply because it has a higher capacity. All models are trained for 100,000 batches on the grayscale training patch set using cosine
annealing~\cite{loshchilov2016sgdr} from a learning rate of $1 \times 10^{-3}$ to $1 \times 10^{-6}$.

\noindent\textbf{Importance of CFM layers.} We emphasized the importance of adaptable weights in the CFM layers, which can be adapted using the quantization matrix. However, there are other simpler methods of using side-channel information. We could simply concatenate the quantization matrix channelwise with the input, or we could ignore the quantization matrix altogether. As shown in the ``CFM'' experiment in Table~\ref{tab:ablation}, the CFM unit performs better than both of these alternatives by a considerable margin. We further visualize the filters learned by the CFM layers and the underlying embeddings in the supplementary material which validate that the learned filters follow a manifold structure.

\noindent\textbf{BlockNet \vs FrequencyNet.} We noted that the FrequencyNet should not be able to perform without a preceding BlockNet because high-frequency information will be zeroed out from the compression process. To test this claim, we train individual BlockNet and FrequencyNet in isolation and report the results in Table~\ref{tab:ablation} (``Subnetworks''). We can see that BlockNet alone attains significantly higher performance than FrequencyNet alone.

\noindent\textbf{Importance of the fusion layer.} Finally, we study the necessity of the fusion layer presented. We posited that the fusion layer was necessary for gradient flow to the early layers of our network. As demonstrated in Table~\ref{tab:ablation} (``Fusion''), the network without fusion fails to learn, matching the input PSNR of classic-5 after full training, whereas the network with fusion makes considerable progress.

