\section{Our Approach}

Our goal is to design a single model capable of JPEG artifact correction at any quality. Towards this, we formulate an architecture that is parameterized by the quantization matrix.

Recall that a JPEG quantization matrix captures the amount of rounding applied to DCT coefficients and is indicative of information lost during compression. A key contribution of our approach is utilizing this quantization matrix directly to guide the restoration process using a fully DCT domain image-to-image regression network. JPEG stores color data in the YCbCr colorspace. The compressed Y channel is much higher quality compared to CbCr channels since human perception is less sensitive to fine color details than to brightness details. Therefore, we follow a staged approach: first restoring artifacts in the Y channel and then using the restored Y channel as guidance to restore the CbCr channels. 

An illustrative overview of our approach is presented in Figure~\ref{fig:overview}. 
Next, we present building blocks utilized in our architecture in $\S$\ref{sec:app:bb}, that allow us to parameterize our model using the quantization matrix and operate entirely in the DCT domain.  Our Y channel and color artifact correction networks are described in $\S$\ref{sec:app:y} and $\S$\ref{sec:app:c} respectively, and finally the training details in $\S$\ref{sec:app:train}.


\subsection{Building Blocks}
\label{sec:app:bb}
By creating a single model capable of JPEG artifact correction at any quality, our model solves a significantly harder problem than previous works. To solve it, we parameterize our network using the $8 \times 8$ quantization matrix available with every JPEG file. We first describe Convolutional Filter Manifolds (CFM), our solution for adaptable convolutional kernels parameterized by the quantization matrix. Since the quantization matrix encodes the amount of rounding per each DCT coefficient, this parameterization is most effective in the DCT domain, a domain where CNNs have previously struggled. Therefore, we also formulate artifact correction as fully DCT domain image-to-image regression and describe critical frequency-relationships-preserving operations.


\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\linewidth]{figures/overview.pdf}
  \caption{\textbf{Overview.} We first restore the Y channel of the input image, then use the restored Y channel to correct
  the color channels which have much worse input quality.}
  \label{fig:overview}
\end{figure}

\noindent\textbf{Convolutional Filter Manifold (CFM).}
Filter Manifolds~\cite{kang2016crowd} were introduced as a way to parameterize a deep CNN using side-channel scalar data. The method learns a manifold of convolutional kernels, which is a function of a scalar input. The manifold is modeled as a three-layer multilayer perceptron. The input to this network is the scalar side-channel data, and the output vector is reshaped to the shape of the desired convolutional kernel and then convolved with the input feature map for that layer.

Recall that in the JPEG compression algorithm, a quantization matrix is derived from a scalar quality setting to determine the amount of rounding to apply, and therefore the amount of information removed from the original image. This quantization matrix is then stored in the JPEG file to allow for correct scaling of the DCT coefficients at decompression time. This quantization matrix is then a strong signal for the amount of information lost. However, the quantization matrix is an $8 \times 8$ matrix with spatial structure, applying the Filter Manifold technique to it has the same drawbacks as processing images with multilayer perceptrons,
\eg a large number of parameters and a lack of spatial relationships.

To solve this, we propose an extension to create Convolutional Filter Manifolds (CFM), replacing the multilayer perceptron by a lightweight three-layer CNN. The input to the CNN is our quantization matrix, and the output is reshaped to the desired convolutional kernel shape and convolved with the input feature map as in the Filter Manifold method. For our problem, we follow the network structure in Figure~\ref{fig:cmf} for each CFM layer. However, this is a general technique and can be used with a different architecture when spatially arranged side-channel data is available. 

\begin{figure}[t]
    \begin{minipage}{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/cmf.pdf}
        \caption{\textbf{Convolutional Filter Manifold}, as used in our network. Note that the convolution with the input
        feature map is done with stride-8.}
        \label{fig:cmf}
    \end{minipage}
    \hspace{0.03\linewidth}
    \begin{minipage}{0.47\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/coef.pdf}
        \caption{\textbf{Coefficient Rearrangement}. Frequencies are arranged channelwise giving an image with 64 times the number of channels
        at $\frac{1}{8}$th the size. This can then be convolved with 64 groups per convolution to learn per-frequency filters.}
        \label{fig:coef}
    \end{minipage}
\end{figure}

\noindent\textbf{Coherent Frequency Operations.}
In prior works, DCT information has been used in dual-domain models~\cite{wang2016d3,zhang2018dmcnn}. These models used standard $3 \times 3$ convolutional kernels with U-Net~\cite{ronneberger2015u} structures to process the coefficients. Although the DCT is a linear map on image pixels~\cite{smith1994fast, ehrlich2019deep}, ablation studies in prior work show that the DCT network alone is not able to surpass even classical artifact correction techniques.

Although the DCT coefficients are arranged in a grid structure of the same shape as the input image, that spatial structure does not have the same meaning as pixels. Image pixels are samples of a continuous signal in two dimensions. DCT coefficients, however, are samples from different, orthogonal functions and the two-dimensional arrangement indexes them. This means that a $3 \times 3$ convolutional kernel is trying to learn a relationship not between spatially related samples of the same function as it was designed to do, but rather between samples from completely unrelated functions. Moreover, it must maintain
this structure throughout the network to produce a valid DCT as output. This is the root cause of CNN's poor performance on DCT coefficients for image-to-image regression, semantic segmentation, and object detection (Note that this should not affect whole image classification performance as in~\cite{gueguen2018faster,ghosh2016deep}).

A class of recent techniques~\cite{deguerre2019fast,lo2019exploring}, which we call Coherent Frequency Operations for their preservation of frequency relationships, are used as the building block for our regression network. The first layer is an $8 \times 8$ stride-8 layer~\cite{deguerre2019fast}, which computes a representation for each block (recall that JPEG blocks are non-overlapping $8\times8$ DCT coefficients). This block representation, which is one eighth the size of the input, can then be processed with a standard CNN. 

The next layer is designed to process each frequency in isolation. Since each of the 64 coefficients in an $8\times8$ JPEG block corresponds to a different frequency, the input DCT coefficients are first rearranged so that the coefficients corresponding to different frequencies are stored channelwise (see Figure~\ref{fig:coef}). This gives an input, which is again one eighth the size of the original image, but this time with 64 channels (one for each frequency). This was referred to as Frequency Component Rearrangement in~\cite{lo2019exploring}. We then use convolutions with 64 groups to learn per-frequency convolutional weights. 

Combining these two operations (block representation using $8\times8$ 8-stride and frequency component rearrangement) allows us to match state-of-the-art pixel and dual-domain results using only DCT coefficients as input and output.

\subsection{Y Channel Correction Network}
\label{sec:app:y}
Our primary goal is artifact correction of full color images, and we again leverage the JPEG algorithm to do this. JPEG stores color data in the YCbCr colorspace. The color channels, which contribute less to the human visual response, are both subsampled and more heavily quantized. Therefore, we employ a larger network to correct only the Y channel, and a smaller network which uses the restored Y channel to more effectively correct the Cb and Cr color channels. 


\begin{figure}[t]
    \begin{minipage}[t]{0.32\linewidth}
        \centering
        \includegraphics[height=0.9\linewidth]{figures/blocknet_abhinav.pdf}
        \caption{\textbf{BlockNet.} Both the block generator and decoder
        are parameterized by the quanitzation matrix.}
        \label{fig:blocknet}
    \end{minipage}
%    \hspace{0.01\linewidth}
    \hfill
    \begin{minipage}[t]{0.3\linewidth}
        \centering
        \includegraphics[height=\linewidth]{figures/frequencynet_abhinav.pdf}
        \caption{\textbf{FrequencyNet.} Note that the 256 channels in the
        RRDB layer actually compute 4 channels per frequency.}
        \label{fig:frequencynet}
    \end{minipage}
        \hfill
%    \hspace{0.01\linewidth}
    \begin{minipage}[t]{0.32\linewidth}
        \centering
        \includegraphics[height=\linewidth]{figures/fusion_abhinav.pdf}
        \caption{\textbf{Fusion subnetwork.} Outputs from all three subnetworks are fused to produce the final residual.}
        \label{fig:fusion}
    \end{minipage}   
\end{figure}

\noindent\textbf{Subnetworks.}
Utilizing the building blocks developed earlier, our network design proceeds in two phases: block enhancement, which learns a quantization invariant representations for each JPEG block, and frequency enhancement, which tries to match each frequency reconstruction to the regression target. These phases are fused to produce the final residual for restoring the Y channel. We employ two purpose-built subnetworks: the block network (BlockNet) and the frequency network (FrequencyNet). Both of these networks can be thought of as separate image-to-image regression models with a structure inspired by ESRGAN~\cite{wang2018esrgan}, which allows sufficient low-level information to be preserved as well as allowing sufficient gradient flow to train these very deep networks. Following recent techniques \cite{wang2018esrgan}, we remove batch normalization layers. While recent works have largely replaced PReLU~\cite{he2015delving} with LeakyReLU~\cite{maas2013rectifier, wang2018esrgan,galteri2017deep,galteri2019deep}, we find that PReLU activations give much higher accuracy.


\noindent\textbf{BlockNet.} This network processes JPEG blocks to restore the Y channel (refer to Figure~\ref{fig:blocknet}). We use
the $8 \times 8$ stride-8 coherent frequency operations to create a block representation. Since this layer is computing a block representation from all the
input DCT coefficients, we use a Convolutional Filter Manifold (CFM) for this layer
so that it has access to quantization information. This allows the layer to learn the quantization table entry to DCT coefficient correspondence with the goal to output a quantization-invariant block representation. Since there is a one to one correspondence between the quantization table entry and rounding applied to a DCT coefficient, this motivates our choice to operate entirely in the DCT domain. We then process these quantization-invariant block representations with Residual-in-Residual Dense Blocks (RRDB) from~\cite{wang2018esrgan}. RRDB layers are an extension of the commonly used residual block~\cite{he2016deep} and define several recursive and highly residual layers. Each RRDB has 15 convolution layers, and we use a single RRDB for the block network with 256 channels. The network terminates with another 
$8 \times 8$ stride-8 CFM, this time transposed, to reverse the block representation back to its original form so that it can be used for later tasks. 


\noindent\textbf{FrequencyNet.} This network, shown in Figure \ref{fig:frequencynet}, processes the individual frequency coefficients using the Frequency Component Rearrangement technique (Figure~\ref{fig:coef}). The architecture of this network is similar to BlockNet. We use a single $3 \times 3$ convolution to change the number of channels from the 64 input channels to the 256 channels used by the RRDB layer. The single RRDB layers processes feature maps with 256 channels and 64 groups yielding 4 channels per frequency. An output $3 \times 3$ convolution transforms the 4 channel output to the 64 output channels, and the coefficients are rearranged back into blocks for later tasks.

\noindent\textbf{Final Network.}
The final Y channel artifact correction network is shown in Figure~\ref{fig:ychannel}. We observe that since the FrequencyNet processes frequency coefficients in isolation, if those coefficients were zeroed out by the compression 
process, then it can make no attempt at restoring them (since they are zero valued they would be set to the layer bias). This is common with high frequencies by design, since they have larger quanitzation table entries and they contribute less to the human visual response. We, therefore, lead with the BlockNet to restore high frequencies. We then pass the result to the FrequencyNet, and its result is then processed by a second block network to restore more information. Finally, a three-layer fusion network (see Figure~\ref{fig:fusion} and~\ref{fig:ychannel}) fuses the output of all three subnetworks into a final result. Having all three subnetworks contribute to the final result in this way allows for better gradient flow. The effect of fusion, as well as the three subnetworks, is tested in our ablation study. The fusion output is treated as a residual and added to the input to produce the final corrected coefficients for the Y channel.


\begin{figure}[t]
    \begin{minipage}[t]{0.43\linewidth}
        \centering
        \includegraphics[width=0.9\columnwidth]{figures/ynetwork_abhinav.pdf}
        \caption{\textbf{Y Channel Network.} We include two copies of the BlockNet, one to perform early restoration of
        high frequency coefficients, and one to work on the restored frequencies. All three
        subnetworks contribute to the final result using the fusion subnetwork.}
        \label{fig:ychannel}
    \end{minipage}
%    \hspace{0.01\linewidth}
\hfill
    \begin{minipage}[t]{0.54\linewidth}
        \centering
        \includegraphics[width=0.9\columnwidth]{figures/colornet_abhinav.pdf}
        \caption{\textbf{Color Channel Network.} Color channels are downsampled, so the block representation is upsampled using a learned upsampling. 
        The Y and color channel block representations are then concatenated to guide the color channel restoration. Cb and Cr channels are processed independently with the
        same network.}
        \label{fig:color}
    \end{minipage}
\end{figure}

\subsection{Color Correction Network}
\label{sec:app:c}
The color channel network (Figure~\ref{fig:color}) processes the Cb and Cr DCT coefficients. Since the color channels are subsampled with respect to the Y channel by half, they incur a much higher loss of information and lose the structural information which is preserved in the Y channel. We first compute the block representation of the downsampled color channel coefficients using a CFM layer, then process them with a single RRDB layer. The block representation is then upsampled using a $4 \times 4$ stride-2  convolutional layer. We compute the block representation of the restored Y channel, again using a CFM layer. The block representations are concatenated channel-wise and processed using a single RRDB layer before being transformed back into coefficient space using a transposed $8 \times 8$ stride-8 CFM. By concatenating the Y channel restoration, we give the network structural information that may be completely missing in the color channels. The result of this network is the color channel residual. This process is repeated individually for each color channel with a single network learned on Cb and Cr. The output residual is added to nearest-neighbor upsampled input coefficients to give the final restoration.

\subsection{Training}
\label{sec:app:train}
\noindent\textbf{Objective.} We use two separate objective functions to train, an error loss and a GAN loss. Our error loss is based on prior works which minimize the $l_1$ error of the result and the target image. We additionally
maximize the Structural Similarity (SSIM)~\cite{wang2004image} of the result since SSIM is generally regarded as a closer metric to human perception than PSNR. This gives our final objective function as 
\begin{equation}
    \label{eq:regressionloss}
    \Lagr_{\text{JPEG}}(x, y) =  \|y - x\|_1 - \lambda\text{SSIM}(x, y)
\end{equation}
where $x$ is the network output, $y$ is the target image, and $\lambda$ is a balancing hyperparameter.

A common phenomenon in JPEG artifact correction and superresolution is the production of a blurry or textureless result. 
To correct for this, we fine tune our fully trained regression network with a GAN loss. For this objective, we use the relativistic 
average GAN loss $\Lagr^{Ra}_G$~\cite{jolicoeur2018relativistic}, we use $l_1$ error to prevent the image from moving too far away from the regression result, and we use preactivation network-based loss~\cite{wang2018esrgan}. Instead of a perceptual loss that tries to keep the outputs close in ImageNet-trained VGG feature space used in prior works, we use a network trained on the MINC dataset~\cite{bell2015material},
for material classification. This texture loss provided only marginal benefit in ESRGAN \cite{wang2018esrgan} for super-resolution. We find it to be critical in our task for restoring texture to blurred regions, since JPEG compression destroys these fine details. The texture loss is defined as 
\begin{equation}
    \Lagr_{\text{texture}}(x, y) = \|\text{MINC}_{5,3}(y) - \text{MINC}_{5,3}(x)\|_1
\end{equation}
where MINC$_{5,3}$ indicates that the output is from layer 5 convolution 3.
The final GAN loss is 
\begin{equation}
    \label{eq:ganloss} 
    \Lagr_\text{GAN}(x, y) = \Lagr_{\text{texture}}(x, y) + \gamma\Lagr^{Ra}_{G}(x, y) + \nu\|x - y\|_1
\end{equation} 
with $\gamma$ and $\nu$ balancing hyperparameters. We note that the texture restored using the GAN model is, in general,
not reflective of the regression target at inference time and actually produces worse numerical results than the regression model despite the images looking more realistic.

\noindent\textbf{Staged Training.} Analogous to our staged restoration, Y channel followed by color channels, we follow a staged training approach. We first train the Y channel correction network using $\Lagr_{\text{JPEG}}$. We then train the color correction network using $\Lagr_{\text{JPEG}}$ keeping the Y channel network weights frozen. Finally, we train the entire network (Y and color correction) with $\Lagr_\text{GAN}$.